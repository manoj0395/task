// routes.js
import Task1 from './components/task1.vue';
import Task2 from './components/task2.vue';
import Task3 from './components/task3.vue';
import Task4 from './components/task4.vue';
import Task5 from './components/task5.vue';
import Task6 from './components/task6.vue';
import Task7 from './components/task7.vue';
import Task8 from './components/task8.vue';

const routes = [
    { path: '/', component: Task1 },
    { path: '/task2', component: Task2 },
    { path: '/task3', component: Task3 },
    { path: '/task4', component: Task4 },
    { path: '/task5', component: Task5 },
    { path: '/task6', component: Task6 },
    { path: '/task7', component: Task7 },
    { path: '/task8', component: Task8 },
];

export default routes;